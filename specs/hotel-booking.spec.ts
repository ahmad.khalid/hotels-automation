import {browser} from "protractor";
import {expect} from "chai";
import {HomePage} from "../pages/homepage/home.page";
import {NavigationComponent} from "../common/navigation.component";
import {StayForm} from "../pages/stayspage/components/stayForm.component";
import {StaySearchComponent} from "../pages/stayspage/components/staySearch.component";
import {HotelsPage} from "../pages/hotelspage/hotels.page";
import {HotelResultsComponent} from "../pages/hotelspage/hotelResults/hotelResults.component";
import {HotelDetailsPage} from "../pages/hoteldetails/hotelDetails.page";
import {ResultFiltersComponent} from "../pages/hotelspage/hotelResults/components/resultFilters.component";
import {RailsComponent} from "../pages/hotelspage/hotelResults/components/rails.component";
import {DealsListComponent} from "../pages/hotelspage/hotelResults/components/dealsList.component";


describe("Step 1", () => {
    const homepage: HomePage = new HomePage();

    before(() => {
        const HOMEPAGE_URL = homepage.getUrl();
        browser.get(HOMEPAGE_URL);
    });

    it('should navigate to stays page', async () => {
        const navigation: NavigationComponent = new NavigationComponent();
        const stayNavItem = navigation.getStayNavItem();
        stayNavItem.click()

        const currentUrl = await browser.getCurrentUrl();
        expect(currentUrl).to.contains("stays");
    });

    const stayForm: StayForm = new StayForm();

    it('should display destination field', async () => {
        const destinationContainer = stayForm.getDestinationContainer();
        const isDestinationContainerPresent = await destinationContainer.isPresent();
        expect(isDestinationContainerPresent).to.be.equal(true);
    });

    it('should display start date field', async () => {
        const startDateField = stayForm.getStartDateField();
        const isStartDatePresent = await startDateField.isPresent();
        expect(isStartDatePresent).to.be.equal(true);
    });

    it('should display end date field', async () => {
        const endDateField = stayForm.getEndDateField();
        const isEndDatePresent = await endDateField.isPresent();
        expect(isEndDatePresent).to.be.equal(true);
    });

    it('should display ‘1 room, 2 guests’ in guests field', async () => {
        const guestsTextField = stayForm.getGuestsFieldText();
        const roomGuests = await guestsTextField.getText();
        expect(roomGuests).to.be.equal('1 room, 2 guests');
    });
});

describe("Step 2", () => {
    it('should load hotelResults results page', async () => {
        let stayForm: StayForm = new StayForm();
        const destinationContainer = stayForm.getDestinationContainer();
        destinationContainer.click();
        await browser.sleep(1000);

        let staySearchComponent: StaySearchComponent = new StaySearchComponent();
        staySearchComponent.getDestinationInputField().sendKeys('BCN');
        await browser.sleep(3000);

        staySearchComponent = new StaySearchComponent();
        const firstSearchItemIndex = 0;
        const searchedItem = staySearchComponent.getDestinationSearchResultItem(firstSearchItemIndex);
        await searchedItem.click()
        await browser.sleep(2000);

        stayForm = new StayForm();
        const searchButton = stayForm.getSearchButton();
        searchButton.click();
        await browser.sleep(3000);

        const hotelsPage: HotelsPage = new HotelsPage();
        const hotelsPageUrl = hotelsPage.getUrl();
        const currentUrl = await browser.getCurrentUrl();
        expect(currentUrl).to.be.contain(hotelsPageUrl)
        await browser.sleep(10000)
    })

    it('should display at least 5 hotel results', async () => {

        const hotelResults: HotelResultsComponent = new HotelResultsComponent();
        const hotelResultsList = hotelResults.getResultsList();
        const hotelResultsLength = await hotelResultsList.count();
        expect(hotelResultsLength).to.be.greaterThan(5)
    });
})

describe("Step 3", () => {


    it('should select first hotel from result', async () => {
        const hotelResults: HotelResultsComponent = new HotelResultsComponent();
        const resultItemIndex = 0;
        const firstResultItem = hotelResults.getResultsItem(resultItemIndex);
        firstResultItem.click();
        await browser.sleep(5000);

        const windowHandles =  await browser.getAllWindowHandles();
        const hotelDetailsPageIndex = 1;
        await browser.switchTo().window(windowHandles[hotelDetailsPageIndex]);
        await browser.sleep(3000);
    })

    it('should display hotel details section', async () => {
        const hotelDetails: HotelDetailsPage = new HotelDetailsPage();
        const hotelDetailsSection = hotelDetails.getDetailsContainer();
        const isDetailsSectionPresent = await hotelDetailsSection.isPresent();
        expect(isDetailsSectionPresent).to.be.equal(true);
    });

    it('should display photos section', async () => {
        const hotelDetails: HotelDetailsPage = new HotelDetailsPage();
        const hotelPhotosSection = hotelDetails.getPhotosContainer();
        const isPhotosSectionPresent = await hotelPhotosSection.isPresent();
        expect(isPhotosSectionPresent).to.be.equal(true);
    })
})

describe('Step 4', () => {
    it('should display map' , async () => {
        const hotelDetails: HotelDetailsPage = new HotelDetailsPage();
        const mapContainer = hotelDetails.getMapContainer();
        const isMapPresent = await mapContainer.isPresent();
        expect(isMapPresent).to.be.equal(true);
    })
})

describe('Step 5', () => {
    it('should display user reviews' , async () => {
        const hotelDetails: HotelDetailsPage = new HotelDetailsPage();
        const userReviewsContainer = hotelDetails.getUserReviewsContainer();
        const areUserReviewsPresent = await userReviewsContainer.isPresent();
        expect(areUserReviewsPresent).to.be.equal(true);
    })
})

describe('Step 6', () => {
    it('should display rates table' , async () => {
        const hotelDetails: HotelDetailsPage = new HotelDetailsPage();
        const ratesTableContainer = hotelDetails.getRatesTableContainer();
        const isRatesTablePresent = await ratesTableContainer.isPresent();
        expect(isRatesTablePresent).to.be.equal(true);
        await browser.close();
        await browser.sleep(3000);
    })
})

describe('Step 7', () => {
    it('should switch to hotel results page', async () => {
        const windowHandles =  await browser.getAllWindowHandles();
        const hotelResultsPageIndex = 0;
        await browser.switchTo().window(windowHandles[hotelResultsPageIndex]);
        await browser.sleep(5000);
    })

    it('should display map view', async () => {
        const resultFilters: ResultFiltersComponent = new ResultFiltersComponent();
        const mapBoxContainer = resultFilters.getMapBoxContainer();
        mapBoxContainer.click();
        await browser.sleep(5000);
    })

    it('should display rails', async () => {
        const railsComponent: RailsComponent = new RailsComponent();
        const rails = railsComponent.getRailsComponent();
        const areRailsPresent = await rails.isPresent();
        expect(areRailsPresent).to.be.equal(true);
        await browser.sleep(2000);
    })
})

describe('Step 8', () => {
    it('should open provider page in the new tab', async () => {
        const dealsList: DealsListComponent = new DealsListComponent();
        const dealIndexNumber = 0;
        const viewDealButton = dealsList.getViewDealButton(dealIndexNumber);
        viewDealButton.click();
        await browser.sleep(2000);

        const windowHandles =  await browser.getAllWindowHandles();
        expect(windowHandles.length).to.be.equal(2);
    });
});