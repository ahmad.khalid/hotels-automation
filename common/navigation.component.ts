import {$, ElementArrayFinder, ElementFinder} from "protractor";
import {NavigationInterface} from "../interfaces/navigation.interface";


export class NavigationComponent {
    navbar: NavigationInterface
    constructor() {
        this.navbar = {
            wrapper: 'div.MvE2',
            nav: {
                container: 'nav.MvE2-nav-container',
                travel: {
                    container: 'div.UVLb',
                    item: 'a.hsCY'
                }
            }
        }
    }

    getWrapper = (): ElementFinder => {
        return $(this.navbar.wrapper);
    }

    getNav = (): ElementArrayFinder => {
        return this.getWrapper().$$(this.navbar.nav.container)
    }
    getTravelNavItems = ():ElementArrayFinder =>{
        const travelNavIndex = 1;
        return this.getNav().get(travelNavIndex).$$(this.navbar.nav.travel.item)
    }
    getStayNavItem = ():ElementFinder => {
        const travelNavItems = this.getTravelNavItems();
        const staysItemIndex = 1;
        return travelNavItems.get(staysItemIndex)
    }
}