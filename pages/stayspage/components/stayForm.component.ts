import {StayFormInterface} from "../../../interfaces/staysPage/stayForm.interface";
import {$, ElementArrayFinder, ElementFinder} from "protractor";
import {StaysPage} from "../stays.page";

export class StayForm extends StaysPage{
    stayForm: StayFormInterface

    constructor() {
        super();
        this.stayForm = {
            container: 'div.x92x-form-fields-and-submit',
            fields: {
                container: 'div.x92x-form-fields',
                destination: {
                    container: 'div.x92x-destination',
                    text: {
                        container: 'div.d_E3',
                        selector: 'div.lNCO-inner'
                    }
                },
                dates: {
                    container: 'div.x92x-dates',
                    selector: 'div.cQtq-input',
                    start: {
                        index: 0
                    },
                    end: {
                        index: 1
                    }
                },
                guests: {
                    container: 'div.x92x-rooms-guests',
                    field: {
                        container: 'div.G2iq-displayContent',
                        text: 'span.G2iq-displayText'
                    }
                }
            },
            search: {
                button: {
                    container: 'div.x92x-submit',
                    selector: 'button[role="button"]'
                },
            }
        }
    }

    getFormContainer = ():ElementFinder => {
        return $(this.stayForm.container)
    }
    getFormFieldsContainer= ():ElementFinder => {
        const formContainer = this.getFormContainer();
        return formContainer.$(this.stayForm.fields.container);
    }
    getDestinationContainer = ():ElementFinder => {
        const fieldsContainer = this.getFormFieldsContainer();
        return fieldsContainer.$(this.stayForm.fields.destination.container);
    }
    getDestinationTextContainer = () => {
        const destinationContainer = this.getDestinationContainer();
        return destinationContainer.$(this.stayForm.fields.destination.text.container);
    }
    getDestinationTextField= () => {
        const destinationTextContainer = this.getDestinationTextContainer();
        return destinationTextContainer.$(this.stayForm.fields.destination.text.selector)
    }
    getDatesContainer = ():ElementFinder => {
        const fieldsContainer = this.getFormFieldsContainer();
        return fieldsContainer.$(this.stayForm.fields.dates.container)
    }
    getDatesFields = ():ElementArrayFinder => {
        const datesContainer = this.getDatesContainer();
        return datesContainer.$$(this.stayForm.fields.dates.selector)
    }
    getStartDateField = ():ElementFinder => {
        const datesFields = this.getDatesFields();
        return datesFields.get(this.stayForm.fields.dates.start.index)
    }
    getEndDateField = ():ElementFinder => {
        const datesFields = this.getDatesFields();
        return datesFields.get(this.stayForm.fields.dates.end.index)
    }
    getGuestsContainer = ():ElementFinder => {
        const fieldsContainer = this.getFormFieldsContainer();
        return fieldsContainer.$(this.stayForm.fields.guests.container);
    }
    getGuestsField = ():ElementFinder => {
        const guestsContainer = this.getGuestsContainer();
        return guestsContainer.$(this.stayForm.fields.guests.field.container)
    }
    getGuestsFieldText = ():ElementFinder => {
        const guestsField = this.getGuestsField();
        return guestsField.$(this.stayForm.fields.guests.field.text);
    }
    getSearchButtonContainer = ():ElementFinder => {
        const formContainer = this.getFormContainer();
        return formContainer.$(this.stayForm.search.button.container);
    }
    getSearchButton = ():ElementFinder => {
        const searchButtonContainer = this.getSearchButtonContainer();
        return searchButtonContainer.$(this.stayForm.search.button.selector)
    }
}