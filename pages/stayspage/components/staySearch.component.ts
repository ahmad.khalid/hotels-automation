import {$, ElementFinder} from "protractor";
import {SearchComponentInterface} from "../../../interfaces/staysPage/searchComponent.interface";
import {StayForm} from "./stayForm.component";

export class StaySearchComponent extends StayForm{
    search: SearchComponentInterface
    constructor() {
        super();
        this.search = {
            wrapper: 'div.c8GSD-wrapper',
            container: 'div.c8GSD-content',
            destination : {
                input: {
                    wrapper: 'div.lNCO-textInputWrapper',
                    selector: 'input.k_my-input'
                },
                results: {
                    wrapper: 'div.c8GSD-overlay-dropdown',
                    resultList: 'ul.QHyi',
                    resultItem: 'li'
                }
            }
            }
    }

    getSearchWrapper = ():ElementFinder => {
        return $(this.search.wrapper)
    }

    getSearchContainer= ():ElementFinder => {
        const searchWrapper = this.getSearchWrapper();
        return searchWrapper.$(this.search.container)
    }

    getDestinationInputWrapper = ():ElementFinder => {
        const searchContainer = this.getSearchContainer();
        return searchContainer.$(this.search.destination.input.wrapper)
    }

    getDestinationInputField = ():ElementFinder => {
        const destinationInputWrapper = this.getDestinationInputWrapper();
        return destinationInputWrapper.$(this.search.destination.input.selector);
    }

    getResultsWrapper = ():ElementFinder => {
        const searchContainer = this.getSearchContainer();
        return searchContainer.$(this.search.destination.results.wrapper)
    }

    getDestinationSearchResultsList = ():ElementFinder => {
        const resultsWrapper = this.getResultsWrapper();
        return resultsWrapper.$(this.search.destination.results.resultList)
    }

    getDestinationSearchResultItem = (itemNumber: number = 0):ElementFinder => {
        const destinationResultsList = this.getDestinationSearchResultsList();
        const resultItems = destinationResultsList.$$(this.search.destination.results.resultItem);
        return resultItems.get(itemNumber);
    }
}