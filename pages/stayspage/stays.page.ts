export class StaysPage{
    url: string;
    constructor() {
        this.url = 'https://www.kayak.com/stays';
    }

    getUrl = ():string => {
        return this.url;
    }
}