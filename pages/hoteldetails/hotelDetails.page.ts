import {$} from "protractor";
import {HotelDetailsInterface} from "../../interfaces/hotelDetails/hotelDetails.interface";

export class HotelDetailsPage {
    hotelDetails: HotelDetailsInterface
    constructor() {
        this.hotelDetails = {
            wrapper: 'div.eu4c',
            details: {
                container: 'div.c3xth',
            },
            photos: {
                container: 'div.c1E0k-photo-container'
            },
            sections: {
                overview: {
                    container: 'div[data-section-name="overview"]'
                },
                rates: {
                    container: 'div[data-section-name="rates"]',
                    table: {
                        container: 'div.Hotels-Results-HotelRoomTypeRatesTableV2'
                    }
                },
                location: {
                    container: 'div[data-section-name="location"]',
                    map: {
                        container: 'div.c9fNw-map'
                    }
                },
                reviews: {
                    container: 'div[data-section-name="reviews"]',
                    userReviews: {
                        container: 'div.l3xK-reviews-container'
                    }
                },
                amenities: {
                    container: 'div[data-section-name="amenities"]'
                },
                policies: {
                    container: 'div[data-section-name="policies"]'
                },
            }
        }
    }

    getDetailsWrapper = () => {
        return $(this.hotelDetails.wrapper);
    }
    getDetailsContainer = () => {
        const detailsWrapper = this.getDetailsWrapper();
        return detailsWrapper.$(this.hotelDetails.details.container)
    }
    getPhotosContainer = () => {
        const detailsWrapper = this.getDetailsWrapper();
        return detailsWrapper.$(this.hotelDetails.photos.container);
    }
    getOverviewContainer = () => {
        const detailsWrapper = this.getDetailsWrapper();
        return detailsWrapper.$(this.hotelDetails.sections.overview.container)
    }
    getRatesContainer = () => {
        const detailsWrapper = this.getDetailsWrapper();
        return detailsWrapper.$(this.hotelDetails.sections.rates.container)
    }
    getRatesTableContainer = () => {
        const ratesContainer = this.getRatesContainer();
        return ratesContainer.$(this.hotelDetails.sections.rates.table.container);
    }
    getLocationContainer = () => {
        const detailsWrapper = this.getDetailsWrapper();
        return detailsWrapper.$(this.hotelDetails.sections.location.container)
    }
    getMapContainer = () => {
        const locationContainer = this.getLocationContainer();
        return locationContainer.$(this.hotelDetails.sections.location.map.container)
    }
    getReviewsContainer = () => {
        const detailsWrapper = this.getDetailsWrapper();
        return detailsWrapper.$(this.hotelDetails.sections.reviews.container)
    }
    getUserReviewsContainer = () => {
        const reviewsContainer = this.getReviewsContainer();
        return reviewsContainer.$(this.hotelDetails.sections.reviews.userReviews.container)
    }
    getAmenitiesContainer = () => {
        const detailsWrapper = this.getDetailsWrapper();
        return detailsWrapper.$(this.hotelDetails.sections.amenities.container)
    }
    getPoliciesContainer = () => {
        const detailsWrapper = this.getDetailsWrapper();
        return detailsWrapper.$(this.hotelDetails.sections.policies.container)
    }
}