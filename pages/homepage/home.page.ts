import {NavigationComponent} from "../../common/navigation.component";

export class HomePage extends NavigationComponent{
    url:string

    constructor() {
        super();
        this.url = 'https://www.kayak.com';
    }

    getUrl = ():string => {
        return this.url;
    }

}