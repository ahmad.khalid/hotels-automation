export class HotelsPage{
    url: string;
    constructor() {
        this.url = 'https://www.kayak.com/hotels';
    }

    getUrl = ():string => {
        return this.url;
    }
}