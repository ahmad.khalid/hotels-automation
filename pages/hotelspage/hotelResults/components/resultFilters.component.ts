import {HotelsPage} from "../../hotels.page";
import {$} from "protractor";
import {ResultFiltersInterface} from "../../../../interfaces/hotelResults/resultFilters.interface";

export class ResultFiltersComponent extends HotelsPage{
    filters: ResultFiltersInterface
    constructor() {
        super();
        this.filters = {
            container: 'div.fArK',
            mapBox: {
                container: 'div.UUVl[aria-label="Click to switch to map view"]'
            }
        }
    }
    getFiltersContainer = () => {
        return $(this.filters.container)
    }
    getMapBoxContainer = () => {
        const filtersContainer = this.getFiltersContainer();
        return filtersContainer.$(this.filters.mapBox.container);
    }
}