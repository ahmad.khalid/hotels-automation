import {HotelResultsComponent} from "../hotelResults.component";
import {MapViewInterface} from "../../../../interfaces/hotelResults/mapView.interface";

export class MapViewComponent extends HotelResultsComponent{
    mapView: MapViewInterface
    constructor() {
        super();
        this.mapView = {
            container: 'div.xh8y',
            selector: 'div.mapboxgl-map'
        }
    }
}