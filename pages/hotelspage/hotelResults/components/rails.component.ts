import {HotelResultsComponent} from "../hotelResults.component";
import {$} from "protractor";
import {RailsInterface} from "../../../../interfaces/hotelResults/rails.interface";

export class RailsComponent extends HotelResultsComponent{
    rail: RailsInterface
    constructor() {
        super();
        this.rail = {
            wrapper: 'div.y8mO',
            container: 'div.y8mO-innerContainer',
            selector: 'div.c_VC0'
        }
    }

    getRailsWrapper = () => {
        return $(this.rail.wrapper)
    }
    getRailsContainer = () => {
        const railsWrapper = this.getRailsWrapper();
        return railsWrapper.$(this.rail.container)
    }
    getRailsComponent = () => {
        const railsContainer = this.getRailsContainer();
        return railsContainer.$(this.rail.selector)
    }

}