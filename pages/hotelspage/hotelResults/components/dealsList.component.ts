import {HotelResultsComponent} from "../hotelResults.component";
import {$} from "protractor";
import {DealsListInterface} from "../../../../interfaces/hotelResults/dealsList.interface";

export class DealsListComponent extends HotelResultsComponent{
    dealsList: DealsListInterface
    constructor() {
        super();
        this.dealsList = {
            container: 'div.lDWm',
            deal: {
                selector: 'div.yuAt',
                view: {
                    selector: 'div.wcw1-clickout'
                }
            }
        }
    }
    getDealsListContainer = () => {
        return $(this.dealsList.container);
    }
    getDeals = () => {
        const dealsListContainer = this.getDealsListContainer();
        return dealsListContainer.$$(this.dealsList.deal.selector)
    }
    getDealItem = (itemNumber: number) => {
        const deals = this.getDeals();
        return deals.get(itemNumber);
    }
    getViewDealButton = (dealNumber: number) => {
        const dealItem = this.getDealItem(dealNumber);
        const firstButtonIndex = 0
        return dealItem.$$(this.dealsList.deal.view.selector).get(firstButtonIndex);
    }
}