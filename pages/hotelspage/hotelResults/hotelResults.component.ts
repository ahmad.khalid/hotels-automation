import {HotelResultsInterface} from "../../../interfaces/hotelResults/hotelResults.interface";
import {$, ElementArrayFinder, ElementFinder} from "protractor";
import {HotelsPage} from "../hotels.page";

export class HotelResultsComponent extends HotelsPage{
    results: HotelResultsInterface
    constructor() {
        super();
        this.results = {
            wrapper: 'div.resultsList',
            container: 'div.Ui-Hotels-Results-List-Components-RheaLiteWrapper-container',
            list: {
                container: 'div.c559F',
                selector: 'div.kzGk'
            },
        }
    }
    getResultsWrapper = ():ElementFinder => {
        return $(this.results.wrapper);
    }

    getResultsContainer = ():ElementFinder => {
        const resultsWrapper = this.getResultsWrapper();
        return resultsWrapper.$(this.results.container)
    }

    getResultsListContainer = ():ElementFinder => {
        const resultsContainer = this.getResultsContainer();
        return resultsContainer.$(this.results.list.container);
    }

    getResultsList = ():ElementArrayFinder => {
        const resultsListContainer = this.getResultsListContainer();
        return resultsListContainer.$$(this.results.list.selector)
    }
    getResultsItem = (itemNumber: number) => {
        const resultsList = this.getResultsList();
        return resultsList.get(itemNumber);
    }
}