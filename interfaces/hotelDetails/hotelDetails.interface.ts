export interface HotelDetailsInterface{
    wrapper: string,
    details: {
        container: string,
    },
    photos: {
        container: string
    },
    sections: {
        overview: {
            container: string
        },
        rates: {
            container: string,
            table: {
                container: string
            }
        },
        location: {
            container: string,
            map: {
                container: string
            }
        },
        reviews: {
            container: string,
            userReviews: {
                container: string
            }
        },
        amenities: {
            container: string
        },
        policies: {
            container: string
        },
    }
}