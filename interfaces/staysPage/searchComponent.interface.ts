export interface SearchComponentInterface{
    wrapper: string,
    container: string,
    destination : {
        input: {
            wrapper: string,
            selector: string
        },
        results: {
            wrapper: string,
            resultList: string,
            resultItem: string
        }
    }
}