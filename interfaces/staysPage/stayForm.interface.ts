export interface StayFormInterface {
    container: string,
    fields: {
        container: string,
        destination: {
            container: string,
            text: {
                container: string,
                selector: string
            }
        },
        dates: {
            container: string,
            selector: string,
            start: {
                index: number
            },
            end: {
                index: number
            }
        },
        guests: {
            container: string,
            field: {
                container: string,
                text: string
            }
        }
    },
    search: {
        button: {
            container: string,
            selector: string
        }
    }
}
