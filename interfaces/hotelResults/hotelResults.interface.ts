export interface HotelResultsInterface{
    wrapper: string,
    container: string,
    list: {
        container: string,
        selector: string
    },
}