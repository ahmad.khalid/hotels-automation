export interface DealsListInterface{
    container: string,
    deal: {
        selector: string,
        view: {
            selector: string
        }
    }
}