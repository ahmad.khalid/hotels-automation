export interface NavigationInterface {
    wrapper: string,
    nav: {
        container: string,
        travel:{
            container: string,
            item: string
        }
    }
}